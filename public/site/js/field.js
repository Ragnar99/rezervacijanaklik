// constants
const monthChange = new Event('monthChange', {"bubbles":true, "cancelable":false});
function AppointmentChoice(){
    this.hourChoice;
    this.dayChoice;
    this.monthChoice;
    this.yearChoice;

    this.getChoice = () => {
        return "" + this.hourChoice + " " + this.dayChoice + " " + this.monthChoice + " " + this.yearChoice;
    }
    this.setChoice = (hourChoice, dayChoice, monthChoice, yearChoice) => {
        this.hourChoice = hourChoice;
        this.dayChoice = dayChoice;
        this.monthChoice = monthChoice;
        this.yearChoice = yearChoice;
    }
}
function TimetableModel(day,month,year){
    this.day = day;
    this.month = month;
    this.year = year;

    // get array for this day
    //this.array = 
    this.available = ['','','','','','','','','','','','','',''];

}
function TimetableView(){
    this.table = document.getElementsByClassName('app-timetable')[0];
    this.array = document.getElementsByClassName('date-hour');

    this.resetArray = () => {
        for(let i = 0; i < this.array.length; i++){
            if(this.array[i].classList.contains('date-hour-taken')){
                this.array[i].classList.remove('date-hour-taken')
            }
            if(this.array[i].classList.contains('date-hour-available')){
                this.array[i].classList.remove('date-hour-available')
            }
            if(this.array[i].classList.contains('date-hour-pick')){
                this.array[i].classList.remove('date-hour-pick')
            }
        }
    }
    this.resetPick = () => {
        for(let i = 0; i < this.array.length; i++){
            if(this.array[i].classList.contains('date-hour-pick')){
                this.array[i].classList.remove('date-hour-pick')
            }
        }
    }

    this.updateArray = (model) => {
    }
}

function CalendarModel(){
    // attributes
    const today = new Date();
    this.day = today.getDate();
    this.month = today.getMonth() + 1;
    this.year = today.getFullYear();

    this.availableDays = { '1' : [],'2' : [],'3' : [],'4' : [],'5' : [],'6' : [],'7' : [],'8' : [],'9' : [], '10' : [],'11' : [], '12' : [],'13' : [],'14' : [],'15' : [],'16' : [],'17' : [],'18' : [], '19' : [],'20' : [],'21' : [],'22' : [],'23' : [],'24' : [],'25' : [], '26' : [],'27' : [],'28' : [],'29' : [],'30' : [],'31' : [] };
    
    // methods
    this.getCurrentDay = () => {
        return this.day; // only this one is int
    }
    this.setCurrentDay = (day) => {
        this.day = day;
    }
    this.getCurrentMonth = () => {
        return this.month.toString();
    }
    this.getCurrentYear = () => {
        return this.year.toString();
    }
    this.getNumberOfDaysInMonth = (month, year) => {
        return new Date(year, month, 0).getDate();
    }
    this.getFirstDay = (month, year) => {
        let date = new Date(year, month - 1, 1);
        date = date.getDay();
        if(date == 0){
            date = 7;
        }
        return date;
    }
    // methods - UI controls
    this.monthForward = () => {
        this.month = (this.month + 1)%12;
        if(this.month == 0){
            this.month = 12;
        } else if(this.month == 1){
            this.year++;
        }
    }
    this.monthBackward = () => {
        this.month = (this.month - 1)%12;
        if(this.month == 0){
            this.month = 12
            this.year--;
        }
    }
}

function CalendarView(){
    // app components
    this.grid = document.getElementsByClassName('date');
    this.dateHeader = document.getElementsByClassName('app-date')[0];
    this.gridHeader = document.getElementById('monthyear');
    this.timetable = document.getElementById('app-timetable');

    this.monthtoString = (month) => {
        const MONTHS = ["","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return MONTHS[month];
    }
    this.updateGrid = (model, month, year) => {
        let dayOfWeek = model.getFirstDay(month, year);
        let numberOfDays = model.getNumberOfDaysInMonth(month, year);
        if(month == 1){
            var previousMonthDays = model.getNumberOfDaysInMonth(12, year-1);
        } else {
            var previousMonthDays = model.getNumberOfDaysInMonth(month - 1, year);
        }
        previousMonthDays = previousMonthDays + 2 - dayOfWeek;
        for(let i = 0; i < dayOfWeek-1; i++, previousMonthDays++){
            this.grid[i].classList.add('lastmonth');
            this.grid[i].innerHTML = previousMonthDays;
        }
        for(let i = dayOfWeek-1, n = 0; n < numberOfDays; i++, n++){
                this.grid[i].innerHTML = n + 1;
        }
        for(let i = numberOfDays+dayOfWeek-1, n = 1; i < grid.length; i++, n++){
            this.grid[i].innerHTML = n;
            this.grid[i].classList.add('nextmonth');
        }
    }
    this.resetGrid = () => {
        for(let i = 0; i < this.grid.length; i++){
            if(this.grid[i].classList.contains("lastmonth")){
                this.grid[i].classList.remove("lastmonth");
            }
            if(this.grid[i].classList.contains("nextmonth")){
                this.grid[i].classList.remove("nextmonth");
            }
            if(this.grid[i].classList.contains("current")){
                this.grid[i].classList.remove("current");
            }
            if(this.grid[i].classList.contains("today")){
                this.grid[i].classList.remove("today");
            }
        }
    }
    this.focusElementWithNumber = (number) => {
        for(let i = 0; i < this.grid.length;i++){
            if(!(this.grid[i].classList.contains("lastmonth")) && !(this.grid[i].classList.contains("nextmonth"))){
                if(this.grid[i].innerHTML == number.toString()){
                    this.grid[i].classList.add('current');
                }
            }
        }
    }
    this.updateDateHeader = (day, month, year) => {
        let rest = '' + this.monthtoString(month) + ' ' + year;
        this.dateHeader.innerHTML = '<span class="date-day" style="font-size: 90px;">' + day + '</span> ' + rest;
    }
    this.updateGridHeader = (month, year) => {
        this.gridHeader.innerHTML = "" + this.monthtoString(month) + " " + year;
    }
    this.updateTimetable = () => {
        console.log("building");
    }
    this.markToday = (today) => {
        for(let i = 0; i < this.grid.length;i++){
            if(!(this.grid[i].classList.contains("lastmonth")) && !(this.grid[i].classList.contains("nextmonth"))){
                if(this.grid[i].innerHTML == today.toString()){
                    this.grid[i].classList.add("today");
                }
            }
        }
    }

}
// three main things
var m = new CalendarModel();
var v = new CalendarView();
var choice = new AppointmentChoice();

// timetable
var tv = new TimetableView();

// event listeners

//tv.array;
for(let i = 0; i < tv.array.length; i++){
    console.log("bbbb");
    tv.array[i].addEventListener("click", () => {
        // update pick
        if(tv.array[i].classList.contains("date-hour-taken")){
            alert("Appointment not available!");// eng
        } else {
            tv.resetPick();
            tv.array[i].classList.add("date-hour-pick");
            choice.setChoice(tv.array[i].innerHTML,m.getCurrentDay(),m.getCurrentMonth(),m.getCurrentYear());
            alert("New choice: " + choice.getChoice());
        }
    });
}
document.getElementById('buttton-next').addEventListener("click", () => {
    let subwindow = document.getElementsByClassName("app-timetable")[0];
    subwindow.innerHTML = "<h1>FORM</h1>";
});
document.addEventListener('DOMContentLoaded', function () {
    v.updateGridHeader(m.getCurrentMonth(), m.getCurrentYear());
    v.resetGrid();
    v.updateGrid(m,m.getCurrentMonth(), m.getCurrentYear());
    v.markToday(m.getCurrentDay());
}, false);
  