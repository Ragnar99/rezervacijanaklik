<?php


namespace App\Enum;


final class ReservationState
{
    const pending = 'pending';
    const accepted = 'accepted';
    const decline = 'decline';

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}