<?php


namespace App\Enum;


final class Day
{
    const Ponedeljak = 'Pon';
    const Utorak = 'Uto';
    const Srijeda = 'Sri';
    const Cetvrtak = 'Čet';
    const Petak = 'Pet';
    const Subota = 'Sub';
    const Nedelja = 'Nedelja';

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}