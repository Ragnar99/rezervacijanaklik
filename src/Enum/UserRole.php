<?php


namespace App\Enum;


use FOS\UserBundle\Model\UserInterface;

final class UserRole
{
    const ROLE_DEFAULT = UserInterface::ROLE_DEFAULT;
    const ROLE_OWNER = 'ROLE_OWNER';
    const ROLE_SUPER_ADMIN = UserInterface::ROLE_SUPER_ADMIN;

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}