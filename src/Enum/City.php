<?php


namespace App\Enum;


final class City
{
    const Bijeljina = 'Bijeljina';
    const Banjaluka = 'Banja Luka';
    const Brcko = 'Brčko';
    const Zvornik = 'Zvornik';
    const IstocniSarajevo = 'Istočno Sarajevo';
    const Trebinje = 'Trebinje';

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}