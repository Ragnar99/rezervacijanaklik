<?php


namespace App\Enum;


final class TableType
{
    const BarskiSto = 'Barski sto';
    const StoSaSjedenjem = 'Sto sa sjedenjem';
    const Secija = 'Secija';

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}