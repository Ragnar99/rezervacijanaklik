<?php


namespace App\Enum;


final class ClubType
{
    const Klub = 'Klub';
    const Restoran = 'Restoran';
    const Pab = 'Pab';
    const Kafana = 'Kafana';
    const Picerija = 'Picerija';
    const Salon = 'Salon';
    const Teren = 'Teren';

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}