<?php


namespace App\Controller\Owner\Gallery;


use App\Enum\UserRole;
use Gumlet\ImageResize;
use App\Entity\Club\Club;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Form\Club\ImagesType;
use App\Service\SaveUploadedFileService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormController extends AbstractController
{
    public function form(ParameterBagInterface $params, Request $request, SaveUploadedFileService $fileService, $id = null){

        $club = $this->getUser()->getClub();
        $images = $club->getImages();

        $form = $this->createForm(ImagesType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $fileService->setTmpFolderPath($params->get('kernel.project_dir') . "/uploads/image/club/");
                $fileService->setUploadedFile($club->getImageOfTables()->getFile());
                /**
                 * @var Image $file
                 */
                foreach ($club->getImages() as $file){

                    $image = $file->getFile();
                    if(!is_null($image)){
                        $imageType = $image->guessExtension();
                        $fileName = md5(uniqid("aaaaa" . '_', false)) . '.' . $imageType;

                        $image->move(
                            $params->get('kernel.project_dir') . "/uploads/image/club/",
                                $fileName 
                        ); 

                        $image = new ImageResize($params->get('kernel.project_dir') . "/uploads/image/club/".$fileName);
                        $image->resize(400, 291);
                        $image->save($params->get('kernel.project_dir') . "/uploads/image/club/" . "1".$fileName);

                        /* $filterManager->applyFilter($file->getFile(), 'club_image'); */

                        $fileService->setUploadedFile("1".$fileName, $imageType);
                        $name = $fileService->call();
                        $file->setFile(null);
                        $file->setName('https://rezervacije.s3.us-east-2.amazonaws.com/' . $name);
                    }

           /*          $fileService->setUploadedFile($file->getFile());
                    $name = $fileService->call();
                    $file->setFile(null);
                    $file->setName('https://rezervacije.s3.us-east-2.amazonaws.com/' . $name); */
                }
/*                 $newArray = new ArrayCollection(array_merge($images->toArray(), $club->getImages()->toArray()));
                $club->setImages($newArray); */
                $em->persist($club);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste dodali klub');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('owner_gallery_list');
        }

        return $this->render('owner/gallery/form.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
    }

    /**
     * @param $id
     * @return Club|object|null
     */
    public function getCLub($id)
    {
        if(is_null($id))
        {
            return new Club();
        }
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        if(is_null($club))
        {
            return new Club();
        }else{
            return $club;
        }
    }
}