<?php


namespace App\Controller\Owner\Gallery;


use App\Entity\Club\Club;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class MainController extends AbstractController
{
    public function main(Request $request, PaginatorInterface $paginator, $id = null)
    {
        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(11); //where approved = false ispraviti

        $club = $this->getUser()->getClub();

        $club->setMain($id);

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($club);
            $em->flush();

            $this->addFlash('success', 'Uspjesno ste postavili glavnu sliku');

        }catch (\Exception $exception) {

            $this->addFlash('error', 'Doslo je do greske');

        }

        return $this->render('owner/gallery/list.html.twig', [
            'club' => $club
        ]);
    }
}