<?php


namespace App\Controller\Owner\Gallery;


use App\Entity\Club\Club;
use App\Entity\Club\Image;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteController extends AbstractController
{
    public function delete(Request $request, PaginatorInterface $paginator, $id = null, $index = null)
    {
        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(11); //where approved = false ispraviti

        $club = $this->getUser()->getClub();

        if($index < $club->getMain()){
            $club->setMain($club->getMain() - 1);
        }

        if($index == $club->getMain()){
            $club->setMain(0);
        }

        $image = $this->getDoctrine()->getRepository(Image::class)->find($id);

        $club->getImages()->removeElement($image);

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($club);
            $em->remove($image);
            $em->flush();

            $this->addFlash('success', 'Uspjesno ste uklonili sliku');

        }catch (\Exception $exception) {

            $this->addFlash('error', 'Doslo je do greske');

        }

        return $this->render('owner/gallery/list.html.twig', [
            'club' => $club
        ]);
    }
}