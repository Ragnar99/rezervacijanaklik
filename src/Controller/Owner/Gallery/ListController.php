<?php


namespace App\Controller\Owner\Gallery;


use App\Entity\Club\Club;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator, $id = null)
    {
        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(11); //where approved = false ispraviti

        $club = $this->getUser()->getClub();

        return $this->render('owner/gallery/list.html.twig', [
            'club' => $club
        ]);
    }
}