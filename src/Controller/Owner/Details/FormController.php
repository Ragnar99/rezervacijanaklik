<?php


namespace App\Controller\Owner\Details;


use App\Entity\Club\Club;
use App\Form\Club\Owner\DetailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{

    public function form(Request $request, $id = null)
    {
        $club = $this->getUser()->getClub();

        $form = $this->createForm(DetailType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($club);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste sacuvali izmjene');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('admin_club_list');
        }

        return $this->render('owner/detail/form.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
    }

    /**
     * @param $id
     * @return Club|object|null
     */
    public function getCLub($id)
    {
        if(is_null($id))
        {
            return new Club();
        }
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        if(is_null($club))
        {
            return new Club();
        }else{
            return $club;
        }
    }
}