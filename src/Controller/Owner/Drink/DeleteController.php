<?php


namespace App\Controller\Owner\Drink;


use App\Entity\Club\Club;
use App\Entity\Drink\Drink;
use App\Form\Drink\DrinkType;
use App\Form\Club\Owner\DetailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteController extends AbstractController
{

    public function delete(Request $request, $id = null)
    {
        $drink = $this->getDoctrine()->getRepository(Drink::class)->find($id);

            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($drink);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste izbrisali');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');
            }

        return $this->redirectToRoute('owner_drink_form');

    }
}