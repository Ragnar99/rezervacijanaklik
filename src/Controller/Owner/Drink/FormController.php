<?php


namespace App\Controller\Owner\Drink;


use App\Entity\Club\Club;
use App\Entity\Drink\Drink;
use App\Form\Drink\DrinkType;
use App\Form\Club\Owner\DetailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormController extends AbstractController
{

    public function form(Request $request, $id = null)
    {
        $club = $this->getUser()->getClub();

        $drink = new Drink();
        $form = $this->createForm(DrinkType::class, $drink);
        $form->handleRequest($request);
        $drink->setClub($club);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($drink);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste sacuvali stavku');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('owner_drink_form');
        }

        return $this->render('owner/drink/form.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
    }
}