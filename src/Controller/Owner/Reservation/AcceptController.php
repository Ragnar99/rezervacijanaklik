<?php


namespace App\Controller\Owner\Reservation;


use App\Entity\Club\Club;
use App\Entity\Party\Table;
use App\Enum\ReservationState;
use App\Entity\Reservation\Reservation;
use App\Entity\Reservation\DayReservation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AcceptController extends AbstractController
{
    public function accept(Request $request, $id = null)
    {

        $user = $this->getUser();

        if($user->getClub()->getType() != "Salon")
        {
            $num = (int) $request->request->get("table");
    
            $reservation = $this->getDoctrine()->getRepository(Reservation::class)->find($id);

            $party = $reservation->getParty();

            $table = $this->getDoctrine()->getRepository(Table::class)->findOneBy(['party' => $party, 'numberOfTable' => $num]);

            //$party->getTables()->remove($table);

            $reservation->setTableNumber($num);
            $reservation->setApproved(true);
            $reservation->setReservationState(ReservationState::accepted);
    /* 
            try { */
                $em = $this->getDoctrine()->getManager();
                $em->persist($reservation);
                $em->remove($table);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste sacuvali izmjene');

    /*         }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            } */
        }else{
            $reservation = $this->getDoctrine()->getRepository(DayReservation::class)->find($id);
            $reservation->setReservationState(ReservationState::accepted);
            $em = $this->getDoctrine()->getManager();
                $em->persist($reservation);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste sacuvali izmjene');
        }

        return $this->redirectToRoute('owner_reservation_list');
    }
}