<?php


namespace App\Controller\Owner\Reservation;


use App\Entity\Club\Club;
use App\Entity\Reservation\Reservation;
use App\Entity\Reservation\DayReservation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator, $id = null)
    {
        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(11);
        $club = $this->getUser()->getClub();

        //$user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        if($club->getType() == "Salon"){
            $qb = $em->getRepository(DayReservation::class)->buildOwnerReservations($club->getId());
        }else{
            $qb = $em->getRepository(Reservation::class)->buildOwnerReservations($club->getId());
        }
        

        //$reservations = $user->getReservations();

        $reservations = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 5
        );

        return $this->render('owner/reservation/list.html.twig', [
            'reservations' => $reservations,
            'club' => $club
        ]);
    }
}