<?php


namespace App\Controller\Owner\Table;


use App\Entity\Club\Club;
use App\Entity\Club\Image;
use App\Form\Club\TableType;
use App\Service\SaveUploadedFileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{

    public function form(ParameterBagInterface $params, Request $request, SaveUploadedFileService $fileService, $id = null){
        $club = $this->getUser()->getClub();

        $form = $this->createForm(TableType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $fileService->setTmpFolderPath($params->get('kernel.project_dir') . "/uploads/image/club/");
                $fileService->setUploadedFile($club->getImageOfTables()->getFile());
                $name = $fileService->call();
                $club->getImageOfTables()->setFile(null);
                $club->getImageOfTables()->setName('https://rezervacije.s3.us-east-2.amazonaws.com/' . $name);
                $em->persist($club);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste dodali klub');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('owner_table_view');
        }

        return $this->render('owner/gallery/form.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
    }

    /**
     * @param $id
     * @return Club|object|null
     */
    public function getCLub($id)
    {
        if(is_null($id))
        {
            return new Club();
        }
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        if(is_null($club))
        {
            return new Club();
        }else{
            return $club;
        }
    }
}