<?php


namespace App\Controller\Owner\Table;


use App\Entity\Club\Club;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ViewController extends AbstractController
{
    public function view(Request $request, PaginatorInterface $paginator, $id = null)
    {
        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(10);
        $club = $this->getUser()->getClub();

        return $this->render('owner/table/view.html.twig', [
            'club' => $club
        ]);
    }
}