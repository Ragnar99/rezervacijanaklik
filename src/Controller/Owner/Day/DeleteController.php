<?php


namespace App\Controller\Owner\Day;


use App\Entity\Club\Club;
use App\Entity\Drink\Drink;
use App\Form\Drink\DrinkType;
use App\Entity\Club\WorkingDay;
use App\Form\Club\Owner\DetailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteController extends AbstractController
{

    public function delete(Request $request, $id = null)
    {
        $day = $this->getDoctrine()->getRepository(WorkingDay::class)->find($id);

            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($day);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste izbrisali');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');
            }

        return $this->redirectToRoute('owner_day_form');

    }
}