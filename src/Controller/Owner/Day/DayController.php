<?php


namespace App\Controller\Owner\Day;


use App\Entity\Club\Club;
use App\Form\Day\DayType;
use App\Entity\Club\WorkingDay;
use App\Form\Club\Owner\DetailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DayController extends AbstractController
{

    public function list(Request $request)
    {
        $club = $this->getUser()->getClub();

        $workingDay = new WorkingDay();
        $workingDay->setClub($club);

        $form = $this->createForm(DayType::class, $workingDay);
        $form->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($workingDay);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste dodali radni dan');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('owner_day_form');
        }

        return $this->render('owner/day/day.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
    }

}