<?php


namespace App\Controller\Owner\Party;

use App\Entity\Club\Club;
use App\Entity\Party\Party;
use App\Entity\Party\Table;
use App\Form\Party\PartyType;
use App\Service\SaveUploadedFileService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormController extends AbstractController
{
    public function form(ParameterBagInterface $params, SaveUploadedFileService $fileService, UserPasswordEncoderInterface $encoder,
                         Request $request, $id = null)
    {
        $party = $this->getParty($id);

        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(11);

        $club = $this->getUser()->getClub();

        $club->getParties()->add($party);

        $party->setClub($club);

        $form = $this->createForm(PartyType::class, $party);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           try {
                $em = $this->getDoctrine()->getManager();
                $fileService->setTmpFolderPath($params->get('kernel.project_dir') . "/uploads/image/club/");
                $fileService->setUploadedFile($party->getImage()->getFile());
                $name = $fileService->call();
                $party->getImage()->setFile(null);
                $party->getImage()->setName('https://rezervacije.s3.us-east-2.amazonaws.com/' . $name);
                for ($i = 1; $i <= $party->getBarTables() + $party->getSeatingTables() + $party->getVipTables(); $i++) {
                    $table = new Table();
                    $table->setNumberOfTable($i);
                    $table->setParty($party);
                    $party->getTables()->add($table);
                    $em->persist($table);
                }
                $em->persist($party);
                $em->persist($club);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste sacuvali zurku');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('owner_party_list');
        }

        return $this->render('owner/party/form.html.twig', [
            'form' => $form->createView(),
            'party' => $party
        ]);
    }

    /**
     * @param $id
     * @return Party|object|null
     */
    public function getParty($id)
    {
        if(is_null($id))
        {
            return new Party();
        }
        $party = $this->getDoctrine()->getRepository(Party::class)->find($id);
        if(is_null($party))
        {
            return new Party();
        }else{
            return $party;
        }
    }
}