<?php


namespace App\Controller\Owner\Party;


use App\Entity\Club\Club;
use App\Entity\Party\Party;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator, $id = null)
    {
        /**
         * @var Club $club
         */
        //$club = $this->getDoctrine()->getRepository(Club::class)->find(11);
        $club = $this->getUser()->getClub();

        return $this->render('owner/party/list.html.twig', [
            'club' => $club
        ]);
    }
}