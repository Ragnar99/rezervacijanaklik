<?php


namespace App\Controller\Owner\Party;

use Exception;
use RuntimeException;
use App\Entity\Club\Club;
use App\Entity\Party\Party;
use App\Entity\Party\Table;
use App\Form\Party\PartyType;
use App\Entity\Reservation\Reservation;
use App\Service\SaveUploadedFileService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ExportUserFromPartyController extends AbstractController
{
    public function export(ParameterBagInterface $params, SaveUploadedFileService $fileService, UserPasswordEncoderInterface $encoder,
                         Request $request, $id = null)
    {
        $party = $this->getParty($id);

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        //$sheet->setCellValue('A1', 'Hello World !');

        //var_dump(sizeof($party->getReservations()));
        $sheet->setTitle("Rezervacije");
        $sheet->setCellValueByColumnAndRow(1, 1, "Ime");
        $sheet->setCellValueByColumnAndRow(2, 1, "Prezime");
        $sheet->setCellValueByColumnAndRow(3, 1, "Tip stola");
        $sheet->setCellValueByColumnAndRow(4, 1, "Broj gostiju");
        $sheet->setCellValueByColumnAndRow(5, 1, "Broj stola");

        //$reservations = $party->getReservations();
        $reservations = $this->getDoctrine()->getRepository(Reservation::class)->getConfirmedReservations($party);
        //var_dump(sizeof($reservations));
        try {
            $row = 2;
            if(!is_null($reservations)){
                foreach ($reservations as $reservation) {
                    $this->fillData($sheet, $row, $reservation);
                    $row++;
                }
            }
        } catch (\Exception $e) {
            throw new RuntimeException('Unknown exception occurred during creating a xls file.', 0, $e);
        }

        $writer = new Xlsx($spreadsheet);

        $fileName = 'rezevarice.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);
        
        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
        
    }

    /**
     * @param $id
     * @return Party|object|null
     */
    public function getParty($id)
    {
        if(is_null($id))
        {
            return new Party();
        }
        $party = $this->getDoctrine()->getRepository(Party::class)->find($id);
        if(is_null($party))
        {
            return new Party();
        }else{
            return $party;
        }
    }


    public function fillData($sheet, $row, $reservation)
    {
        $sheet->setCellValueByColumnAndRow(1, $row,  $reservation->getUser()->getFirstName());
        $sheet->setCellValueByColumnAndRow(2, $row,  $reservation->getUser()->getLastName());
        $sheet->setCellValueByColumnAndRow(3, $row,  $reservation->getTableType());
        $sheet->setCellValueByColumnAndRow(4, $row,  $reservation->getGuestNumber());
        $sheet->setCellValueByColumnAndRow(5, $row,  $reservation->getTableNumber());
    }
}