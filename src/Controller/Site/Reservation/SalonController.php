<?php


namespace App\Controller\Site\Reservation;


use DateTime;
use App\Enum\UserRole;
use App\Entity\Club\Club;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Entity\Party\Party;
use GuzzleHttp\Psr7\UploadedFile;
use App\Entity\Reservation\Reservation;
use App\Service\SaveUploadedFileService;
use App\Form\Reservation\ReservationType;
use App\Entity\Reservation\DayReservation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SalonController extends AbstractController
{
    public function reservation($id = null)
    {
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        $datetime = new DateTime();
        $reservations = $this->getDoctrine()->getRepository(DayReservation::class)->getAllReservations($datetime, $id);

        $list = [];
        foreach($reservations as $reservation)
        {
            $list[$reservation->getStringDate()] = true;
        }
        return $this->render('site/reservation/salon.html.twig',[
            'club' => $club,
            'list' => json_encode($list)
        ]);
    }

}