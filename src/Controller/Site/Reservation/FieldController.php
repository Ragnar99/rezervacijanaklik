<?php


namespace App\Controller\Site\Reservation;


use App\Enum\UserRole;
use App\Entity\Club\Club;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Entity\Party\Party;
use GuzzleHttp\Psr7\UploadedFile;
use App\Entity\Reservation\Reservation;
use App\Service\SaveUploadedFileService;
use App\Form\Reservation\ReservationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FieldController extends AbstractController
{
    public function reservation()
    {
        $club = $this->getDoctrine()->getRepository(Club::class)->find(26);
        return $this->render('site/reservation/field.html.twig',[
            'club' => $club
        ]);
    }

}