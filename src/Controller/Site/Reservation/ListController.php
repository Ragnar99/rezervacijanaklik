<?php


namespace App\Controller\Site\Reservation;


use App\Enum\UserRole;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Entity\Party\Party;
use GuzzleHttp\Psr7\UploadedFile;
use App\Entity\Reservation\Reservation;
use App\Service\SaveUploadedFileService;
use App\Form\Reservation\ReservationType;
use App\Entity\Reservation\DayReservation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ListController extends AbstractController
{
    public function list(PaginatorInterface $paginator,
                         Request $request, $id = null, $type = null)
    {

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        if($type == "klub"){
            $qb = $em->getRepository(Reservation::class)->buildUserReservations($user->getId());
        }else{
            $qb = $em->getRepository(DayReservation::class)->buildUserReservations($user->getId());
        }

        //$reservations = $user->getReservations();

        $reservations = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 5
        );


        return $this->render('site/reservation/list.html.twig', [
            'reservations' => $reservations,
            'type' => $type
        ]);
    }

}