<?php


namespace App\Controller\Site\Reservation;


use DateTime;
use App\Enum\UserRole;
use App\Entity\Club\Club;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Entity\Party\Party;
use GuzzleHttp\Psr7\UploadedFile;
use App\Entity\Reservation\Reservation;
use App\Service\SaveUploadedFileService;
use App\Form\Reservation\ReservationType;
use App\Entity\Reservation\DayReservation;
use App\Form\Reservation\DayReservationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DayController extends AbstractController
{
    public function reservation(Request $request, $id = null, $date)
    {
        
        $dateTime = date_create_from_format('Y-m-d', $date);
    
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id); //repraviti u $id
        $user = $this->getUser();

            $reservation = new DayReservation();
            $reservation->setDatetime($dateTime);
            $reservation->setStringDate($date);
            $reservation->setClub($club);
            $reservation->setUser($user);
            $form = $this->createForm(DayReservationType::class, $reservation);
            $form->handleRequest($request);

    
       
        //var_dump($reservation->getDatetime());
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($reservation);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste kreirali zahtjev za rezervaciju');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('site_reservation_list');
        }

        return $this->render('site/reservation/day_form.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
        
    }

}