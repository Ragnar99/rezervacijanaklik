<?php


namespace App\Controller\Site\Reservation;


use App\Enum\UserRole;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Entity\Party\Party;
use GuzzleHttp\Psr7\UploadedFile;
use App\Entity\Reservation\Reservation;
use App\Service\SaveUploadedFileService;
use App\Form\Reservation\ReservationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormController extends AbstractController
{
    public function form(ParameterBagInterface $params, SaveUploadedFileService $fileService, UserPasswordEncoderInterface $encoder,
                         Request $request, $id = null)
    {

        $user = $this->getUser();
        $reservation = new Reservation();
        $party = $this->getDoctrine()->getRepository(Party::class)->find($id);
        $reservation->setParty($party);
        $reservation->setDatetime($party->getDate());
        $reservation->setUser($user);
        $user->getReservations()->add($reservation);

        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($reservation);
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste kreirali zahtjev za rezervaciju');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('site_reservation_list');
        }

        return $this->render('site/reservation/form.html.twig', [
            'form' => $form->createView(),
            'club' => $reservation->getParty()->getClub()
        ]);
    }

}