<?php


namespace App\Controller\Site\Party;


use DateTime;
use App\Entity\Club\Club;
use App\Entity\Party\Party;
use App\Form\Club\SiteClubFilterType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{
    public function list(Request $request,PaginatorInterface $paginator, $id = null)
    {
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        $date = new DateTime();
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->getRepository(Party::class)->buildClubsByType($club, $date);
        
        $parties = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 10
        );

        return $this->render('site/party/list.html.twig', [
            'parties' => $parties
        ]);
    }
}