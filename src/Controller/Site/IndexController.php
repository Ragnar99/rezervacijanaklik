<?php


namespace App\Controller\Site;


use App\Entity\Club\Club;
use App\Form\Search\GlobalSearchForm;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    public function index(Request $request, PaginatorInterface $paginator)
    {

        $clubs = $this->getDoctrine()->getRepository(Club::class)->findAll();

        return $this->render('site/index.html.twig', [
            'club1' => $clubs[0],
            'club2' => $clubs[1],
            'club3' => $clubs[2]
        ]);
    }
}