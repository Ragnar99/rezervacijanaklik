<?php


namespace App\Controller\Site\Drink;


use App\Entity\Club\Club;
use App\Entity\Drink\Drink;
use App\Form\Drink\DrinkType;
use App\Form\Club\Owner\DetailType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{

    public function list(Request $request, PaginatorInterface $paginator, $id = null)
    {
        $em = $this->getDoctrine()->getManager();

        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
 
        $qb = $em->getRepository(Drink::class)->buildDrinks($id);

        $drinks = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 15
        );

        return $this->render('site/drink/list.html.twig', [
            'club' => $club,
            'drinks' => $drinks
        ]);
    }

    /**
     * @param $id
     * @return Club|object|null
     */
    public function getClub($id)
    {
        if(is_null($id))
        {
            return new Club();
        }
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        if(is_null($club))
        {
            return new Club();
        }else{
            return $club;
        }
    }
}