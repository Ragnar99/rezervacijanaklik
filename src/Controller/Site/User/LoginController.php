<?php


namespace App\Controller\Site\User;


use App\Entity\Reservation\Reservation;
use App\Entity\Club\Image;
use App\Entity\User\User;
use App\Enum\UserRole;
use App\Form\Reservation\ReservationType;
use App\Service\SaveUploadedFileService;
use GuzzleHttp\Psr7\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginController extends AbstractController
{
    public function login(ParameterBagInterface $params, SaveUploadedFileService $fileService, UserPasswordEncoderInterface $encoder,
                         Request $request, $id = null)
    {

        return $this->render('site/registration/login.html.twig');
    }

}