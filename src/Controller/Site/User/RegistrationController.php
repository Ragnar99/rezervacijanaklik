<?php


namespace App\Controller\Site\User;


use App\Entity\Reservation\Reservation;
use App\Entity\Club\Image;
use App\Entity\User\User;
use App\Enum\UserRole;
use App\Form\User\RegistrationType;
use App\Service\SaveUploadedFileService;
use GuzzleHttp\Psr7\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    public function registration(ParameterBagInterface $params, SaveUploadedFileService $fileService, UserPasswordEncoderInterface $encoder,
                         Request $request, $id = null)
    {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoded);
                $user->setUsername($user->getEmail());
                $user->setEnabled(true);
                $user->setRoles([UserRole::ROLE_DEFAULT]);
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Uspjesno ste se registrovali');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('site_index');
        }

        return $this->render('site/registration/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

}