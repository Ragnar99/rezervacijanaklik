<?php


namespace App\Controller\Site\Club;


use App\Entity\Club\Club;
use App\Form\Club\SiteClubFilterType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator, $type = null, $term = null, $city = null)
    {

        $em = $this->getDoctrine()->getManager();

        if(is_null($term) && is_null($city)){
            if($type == "None") { $type = null; }
            if(is_null($type)){
                $qb = $em->getRepository(Club::class)->buildClubs();
            }else{
                $qb = $em->getRepository(Club::class)->buildClubsByType($type);
            }
        }else{
            $qb = $em->getRepository(Club::class)->buildSearchClub($term, $city);
        }
        
        $form = $this->createForm(SiteClubFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $city = $form->getData()['city'];
            $type = $form->getData()['type'];
            $qb   = $em->getRepository(Club::class)->buildFilter($city, $type);
        }

        

        $clubs = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 4
        );

        return $this->render('site/club/list.html.twig', [
            'form' => $form->createView(),
            'type' => $type,
            'clubs' => $clubs
        ]);
    }
}