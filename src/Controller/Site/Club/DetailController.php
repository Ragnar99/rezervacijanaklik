<?php


namespace App\Controller\Site\Club;

use App\Entity\Club\Club;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DetailController extends AbstractController
{
    public function detail(Request $request, $id = null)
    {

        $club= $this->getDoctrine()->getRepository(Club::class)->find($id);
   
        return $this->render('site/club/detail.html.twig', [
            'club' => $club
        ]);
        
    }

}