<?php


namespace App\Controller\Site\Club;


use App\Entity\Club\Club;
use App\Form\Club\SiteClubFilterType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    public function search(Request $request, PaginatorInterface $paginator, $type = null)
    {

        $term = $request->request->get("term");
        $city = $request->request->get("city");

        return $this->redirectToRoute('site_club_list', ['type'=>'None', 'term' => $term, 'city' => $city]);



/*         $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository(Club::class)->buildSearchClub($term, $city);

        $form = $this->createForm(SiteClubFilterType::class);

        $form->handleRequest($request);

        $clubs = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 4
        );

        return $this->render('site/club/list.html.twig', [
            'type' => 'Nije definisan',
            'clubs' => $clubs
        ]); */
    }
}