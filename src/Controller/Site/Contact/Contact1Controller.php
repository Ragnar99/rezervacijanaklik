<?php


namespace App\Controller\Site\Contact;


use App\Entity\Club\Club;
use App\Entity\Drink\Drink;
use App\Form\Drink\DrinkType;
use App\Form\Club\Owner\DetailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Contact1Controller extends AbstractController
{

    public function contact(Request $request, $id = null)
    {
        return $this->render('site/contact/contact.html.twig', [
        ]);
    }

}