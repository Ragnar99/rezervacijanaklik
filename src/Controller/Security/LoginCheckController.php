<?php

namespace App\Controller\Security;

use App\Entity\User\User;
use App\Enum\UserRoles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginCheckController extends Controller
{

    public function LoginCheckAction()
    {
        $router = $this->container->get('router');

        /**
         * @var User $user
         */
        $user = $this->getUser();

        return new RedirectResponse($router->generate('site_index'), 307);
       /*  $type = $user->getType();

     

        
        if ($type === UserTypes::STORE) {
            return new RedirectResponse($router->generate('store_dashboard'), 307);
        }

        if ($type === UserTypes::INFLUENCER) {
            return new RedirectResponse($router->generate('store_dashboard'), 307);
        }

        if ($type === UserTypes::BRAND) {
            return new RedirectResponse($router->generate('brand_dashboard'), 307);
        }

        if ($type === UserTypes::INTERNAL) {
            return new RedirectResponse($router->generate('admin_dashboard'), 307);
        } */


    }

}