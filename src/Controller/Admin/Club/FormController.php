<?php


namespace App\Controller\Admin\Club;

use App\Enum\UserRole;
use \Gumlet\ImageResize;
use App\Entity\Club\Club;
use App\Entity\User\User;
use App\Entity\Club\Image;
use App\Form\Club\ClubType;
use App\Entity\Club\WorkingDay;
use App\Entity\Utils\SimpleImage;
use GuzzleHttp\Psr7\UploadedFile;
use App\Service\SaveUploadedFileService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormController extends AbstractController
{
    public function form(ParameterBagInterface $params, SaveUploadedFileService $fileService, UserPasswordEncoderInterface $encoder,
                        CacheManager $cacheManager, FilterManager $filterManager, DataManager $dataManager, Request $request, $id = null)
    {
        $club = $this->getClub($id);

        $form = $this->createForm(ClubType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
/*             try { */

                $club->setMain(0);
                $em = $this->getDoctrine()->getManager();
                /**
                 * @var User $user
                 */
                $user = $club->getUser();
                $user->setClub($club);
                $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoded);
                $user->setUsername($user->getEmail());
                $user->setEnabled(true);
                $user->setRoles([UserRole::ROLE_OWNER]);
                 $fileService->setTmpFolderPath($params->get('kernel.project_dir') . "/uploads/image/club/");
                /*$fileService->setUploadedFile($club->getImageOfTables()->getFile(), 'jpg'); */
                /* $name = $fileService->call(); */
               /*  $club->getImageOfTables()->setFile(null);
                $club->getImageOfTables()->setName('https://rezervacije.s3.us-east-2.amazonaws.com/' . $name);
                 */
                
                /**
                 * @var Image $file
                 */
                foreach ($club->getImages() as $file){
                
                     $image = $file->getFile();
                     $imageType = $image->guessExtension();
                     $fileName = md5(uniqid("aaaaa" . '_', false)) . '.' . $imageType;

                      $image->move(
                        $params->get('kernel.project_dir') . "/uploads/image/club/",
                            $fileName 
                        ); 
                        //. "/uploads/image/club/"

                    //$image = $dataManager->find( 'club_image', $fileName);
                    //$image = $filterManager->applyFilter( $image, 'club_image'); 

                    //$cacheManager->store( $image, $fileName, 'club_image', 'local_storage' );

                    //$url = $imagineCacheManager->resolve( $fileName, 'club_image' ); 
                    
                    /** @var string */
                    //$resolvedPath = $cacheManager->getBrowserPath($params->get('kernel.project_dir') . "/uploads/image/club/" . $fileName, 'club_image');
                

                    //$newFile = new File($image);

                    //$image1 = new SimpleImage($params->get('kernel.project_dir') . "/uploads/image/club/".$fileName); 
                    //$image->load($image);
                    //$image1->resize(400,291); 
                    //$image1->save($params->get('kernel.project_dir') . "/uploads/image/club/" . "1".$fileName);
                
                    $image = new ImageResize($params->get('kernel.project_dir') . "/uploads/image/club/".$fileName);
                    $image->resize(400, 291);
                    $image->save($params->get('kernel.project_dir') . "/uploads/image/club/" . "1".$fileName);

                    /* $filterManager->applyFilter($file->getFile(), 'club_image'); */

                    $fileService->setUploadedFile("1".$fileName, $imageType);
                    $name = $fileService->call();
                    $file->setFile(null);
                    $file->setName('https://rezervacije.s3.us-east-2.amazonaws.com/' . $name);
                }
                $em->persist($club);
                $em->persist($user);
                $this->addAllDays($club, $em);
                $em->flush();

/*                 $this->addFlash('success', 'Uspjesno ste dodali klub');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'Doslo je do greske');

            }

            return $this->redirectToRoute('admin_club_list'); */
        }

        return $this->render('admin/club/form.html.twig', [
            'form' => $form->createView(),
            'club' => $club
        ]);
    }

    /**
     * @param $id
     * @return Club|object|null
     */
    public function getCLub($id)
    {
        if(is_null($id))
        {
            return new Club();
        }
        $club = $this->getDoctrine()->getRepository(Club::class)->find($id);
        if(is_null($club))
        {
            return new Club();
        }else{
            return $club;
        }
    }

    private function addAllDays($club, $em)
    {
        foreach (\App\Enum\Day::all() as $key) {
            $wokingDay = new WorkingDay();

            $workingDay->setClub($club);
            $workingDay->setDay($key);

            $em->persist($workingDay);
        }
    }
}