<?php


namespace App\Controller\Admin\Club;


use App\Entity\Club\Club;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator)
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository(Club::class)->buildClubs();

        $clubs = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 10
        );

        return $this->render('admin/club/list.html.twig', [
            'clubs' => $clubs
        ]);
    }
}