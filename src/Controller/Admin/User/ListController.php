<?php


namespace App\Controller\Admin\User;


use App\Entity\Club\Club;
use App\Entity\User\User;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator)
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository(User::class)->buildUsers();

        $users = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 10
        );

        return $this->render('admin/user/list.html.twig', [
            'users' => $users
        ]);
    }
}