<?php


namespace App\Entity\Reservation;

use App\Enum\ReservationState;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Reservation\DayReservationRepository")
 * @ORM\Table(name="day_reservation")
 */
class DayReservation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="dayReservation")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $reservationState = ReservationState::pending;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $guestNumber;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $stringDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Club\Club", inversedBy="dayReservations")
     */
    private $club;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime|null
     */
    public function getDatetime(): ?\DateTime
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime
     */
    public function setDatetime(\DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

     /**
     * @return int|null
     */
    public function getGuestNumber(): ?int
    {
        return $this->guestNumber;
    }

    /**
     * @param int $guestNumber
     */
    public function setGuestNumber(int $guestNumber): void
    {
        $this->guestNumber = $guestNumber;
    }

    /**
     * @return string|null
     */
    public function getReservationState(): ?string
    {
        return $this->reservationState;
    }

    /**
     * @param string $state
     */
    public function setReservationState(string $state): void
    {
        $this->reservationState = $state;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getStringDate(): ?string
    {
        return $this->stringDate;
    }

    /**
     * @param string $stringDate
     */
    public function setStringDate(string $stringDate): void
    {
        $this->stringDate = $stringDate;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     */
    public function setClub($club): void
    {
        $this->club = $club;
    }

}