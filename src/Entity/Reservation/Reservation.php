<?php


namespace App\Entity\Reservation;

use App\Enum\ReservationState;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Reservation\ReservationRepository")
 * @ORM\Table(name="reservation")
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="reservation")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tableNumber;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $condition1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Party\Party", inversedBy="reservations")
     */
    private $party;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $guestNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $approved = false;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $tableType;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $reservationState = ReservationState::pending;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return int|null
     */
    public function getTableNumber(): ?int
    {
        return $this->tableNumber;
    }

    /**
     * @param int $tableNumber
     */
    public function setTableNumber(int $tableNumber): void
    {
        $this->tableNumber = $tableNumber;
    }

    /**
     * @return \DateTime|null
     */
    public function getDatetime(): ?\DateTime
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime
     */
    public function setDatetime(\DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * @return mixed
     */
    public function getParty()
    {
        return $this->party;
    }

    /**
     * @param mixed $party
     */
    public function setParty($party): void
    {
        $this->party = $party;
    }

    /**
     * @return int|null
     */
    public function getGuestNumber(): ?int
    {
        return $this->guestNumber;
    }

    /**
     * @param int $guestNumber
     */
    public function setGuestNumber(int $guestNumber): void
    {
        $this->guestNumber = $guestNumber;
    }

    /**
     * @return string|null
     */
    public function getCondition1(): ?string
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     */
    public function setCondition1(string $condition): void
    {
        $this->condition1 = $condition;
    }

    /**
     * @return bool|null
     */
    public function isApproved(): ?bool
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     */
    public function setApproved(bool $approved): void
    {
        $this->approved = $approved;
    }

    /**
     * @return string|null
     */
    public function getTableType(): ?string
    {
        return $this->tableType;
    }

    /**
     * @param string $tableType
     */
    public function setTableType(string $tableType): void
    {
        $this->tableType = $tableType;
    }

    /**
     * @return string|null
     */
    public function getReservationState(): ?string
    {
        return $this->reservationState;
    }

    /**
     * @param string $state
     */
    public function setReservationState(string $state): void
    {
        $this->reservationState = $state;
    }

}