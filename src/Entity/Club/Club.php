<?php


namespace App\Entity\Club;

use DateTime;
use App\Entity\Club\Image;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Club\ClubRepository")
 * @ORM\Table(name="club")
 */
class Club
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $address;

    /**
     * @var double
     *
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $longitude;

    /**
     * @var double
     *
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $city;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $barTables;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $seatingTables;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vipTables;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $premium;

    /**
     * @var Image
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Club\Image", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Type(type="App\Entity\Club\Image",)
     * @Assert\Valid()
     */
    private $imageOfTables;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Club\Image", cascade={"persist"})
     * @ORM\JoinTable(name="club_images",
     *    joinColumns={@ORM\JoinColumn(name="club_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $images;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Party\Party", mappedBy="club")
     */
    private $parties;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Drink\Drink", mappedBy="club")
     */
    private $drinks;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\User", inversedBy="club", cascade={"all"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $webSite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $startWorking;

    /**
    * @var \DateTime
    *
    * @ORM\Column(type="datetime")
    */
    private $endWorking;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $header;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     */
    private $description;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $main = 0;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Club\WorkingDay", mappedBy="club")
     */
    private $workingDays;

       /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation\DayReservation", mappedBy="club")
     */
    private $dayReservations;

    /**
     * Club constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->parties = new ArrayCollection();
        $this->drinks = new ArrayCollection();
        $this->workingDays = new ArrayCollection();
        $this->dayReservations = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return int|null
     */
    public function getBarTables(): ?int
    {
        return $this->barTables;
    }

    /**
     * @param int $barTables
     */
    public function setBarTables(int $barTables): void
    {
        $this->barTables = $barTables;
    }

    /**
     * @return int|null
     */
    public function getSeatingTables(): ?int
    {
        return $this->seatingTables;
    }

    /**
     * @param int $seatingTables
     */
    public function setSeatingTables(int $seatingTables): void
    {
        $this->seatingTables = $seatingTables;
    }

    /**
     * @return int|null
     */
    public function getVipTables(): ?int
    {
        return $this->vipTables;
    }

    /**
     * @param int $vipTables
     */
    public function setVipTables(int $vipTables): void
    {
        $this->vipTables = $vipTables;
    }

    /**
     * @return bool|null
     */
    public function isPremium(): ?bool
    {
        return $this->premium;
    }

    /**
     * @param bool $premium
     */
    public function setPremium(bool $premium): void
    {
        $this->premium = $premium;
    }

    /**
     * @return \App\Entity\Club\Image|null
     */
    public function getImageOfTables(): ?\App\Entity\Club\Image
    {
        return $this->imageOfTables;
    }

    /**
     * @param \App\Entity\Club\Image $imageOfTables
     */
    public function setImageOfTables(\App\Entity\Club\Image $imageOfTables): void
    {
        $this->imageOfTables = $imageOfTables;
    }

    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages(ArrayCollection $images): void
    {
        $this->images = $images;
    }

    public function getParties()
    {
        return $this->parties;
    }

    /**
     * @param ArrayCollection $parties
     */
    public function setParties(ArrayCollection $parties): void
    {
        $this->parties = $parties;
    }

    public function getDrinks()
    {
        return $this->drinks;
    }

    /**
     * @param ArrayCollection $drinks
     */
    public function setDrinks(ArrayCollection $drinks): void
    {
        $this->drinks = $drinks;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function getAttachDocument()
    {
        return null;
    }

    /**
     * @param null $file
     * @return array|null
     * @throws \Exception
     */
    public function setAttachDocument($file = null)
    {
        if (!$file) return [];
        $this->attachDocument($file);
        return null;
    }

    /**
     * @param UploadedFile|null $file
     * @throws \Exception
     */
    public function attachDocument(UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }
        $image = new Image();
        $image->setFile($file);
        $this->imageOfTables = $image;
    }

    /**
     * @return null
     */
    public function getAttachPictures()
    {
        return null;
    }

    /**
     * @param array $files
     * @return array
     */
    public function setAttachPictures(array $files = array())
    {
        if (!$files) return [];
        foreach ($files as $file) {
            if (!$file) return [];
            $this->attachPicture($file);
        }
        return [];
    }

    /**
     * @param UploadedFile|null $file
     */
    public function attachPicture(UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }
        $picture = new Image();
        $picture->setFile($file);
        $this->addPicture($picture);
    }

    /**
     * @param Image $picture
     */
    public function addPicture(Image $picture)
    {
        $this->images->add($picture);
    }

    /**
     * @param Image $picture
     */
    public function removePicture(Image $picture)
    {
        $this->images->removeElement($picture);
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $name
     */
    public function setPhone(string $name): void
    {
        $this->phone = $name;
    }

     /**
     * @return string|null
     */
    public function getRating(): ?string
    {
        return $this->rating;
    }

    /**
     * @param string $name
     */
    public function setRating(string $name): void
    {
        $this->rating = $name;
    }

     /**
     * @return string|null
     */
    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    /**
     * @param string $name
     */
    public function setWebSite(string $name): void
    {
        $this->webSite = $name;
    }
    
     /**
     * @return string|null
     */
    public function getHeader(): ?string
    {
        return $this->header;
    }

    /**
     * @param string $name
     */
    public function setHeader(string $name): void
    {
        $this->header = $name;
    }

     /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $name
     */
    public function setDescription(string $name): void
    {
        $this->description = $name;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartWorking(): ?\DateTime
    {
        return $this->startWorking;
    }

    /**
     * @param \DateTime $date
     */
    public function setStartWorking(\DateTime $date): void
    {
        $this->startWorking = $date;
    }

     /**
     * @return \DateTime|null
     */
    public function getEndWorking(): ?\DateTime
    {
        return $this->endWorking;
    }

    /**
     * @param \DateTime $date
     */
    public function setEndWorking(\DateTime $date): void
    {
        $this->endWorking = $date;
    }

     /**
     * @return int|null
     */
    public function getMain(): ?int
    {
        return $this->main;
    }

    /**
     * @param int $barTables
     */
    public function setMain(int $barTables): void
    {
        $this->main = $barTables;
    }

    public function getWorkingDays()
    {
        return $this->workingDays;
    }

    /**
     * @param ArrayCollection $workingDays
     */
    public function setWorkingDays(ArrayCollection $workingDays): void
    {
        $this->workingDays = $workingDays;
    }

    public function getDayReservations()
    {
        return $this->dayReservations;
    }

    /**
     * @param ArrayCollection $dayReservations
     */
    public function setDayReservations(ArrayCollection $dayReservations): void
    {
        $this->dayReservations = $dayReservations;
    }

    public function isOpen()
    {
        $currentDay = new DateTime();
        $hours = $currentDay->format('H');
        $minutes = $currentDay->format('i');
        $dayOfWeek = $currentDay->format('l');

        $startTimeHours = $this->startWorking->format('H');
        $startTimeMinutes = $this->startWorking->format('i');

        $endTimeHours = $this->endWorking->format('H');
        $endTimeMinutes = $this->endWorking->format('i');

        $start = $startTimeHours * 60 + $startTimeMinutes;
        $end = $endTimeHours * 60 + $endTimeMinutes;

        $currentDate = $hours * 60 + $minutes;

        $flag = false;
        $day = $this->getDayOfWeekOnSerbian($dayOfWeek);

        foreach($this->workingDays as $workingDay)
        {
            if($day == $workingDay->getDay())
            {
                $flag = true;
                break;
            }
        }

        if($flag){
            if($start < $end)
            {
                if($start <= $currentDate && $end >= $currentDate)
                {
                    return "OPEN NOW";
                }else{
                    return "CLOSED NOW";
                }
            }else{

                $night = 23 * 60 + 59;
                if($night > $currentDate){
                    if($start <= $currentDate && $end <= $currentDate)
                    {
                        return "OPEN NOW";
                    }else{
                        return "CLOSED NOW";
                    }
                }else{
                    if($start >= $currentDate && $end >= $currentDate)
                    {
                        return "OPEN NOW";
                    }else{
                        return "CLOSED NOW";
                    }
                }

                
            }
        }else{
            return "CLOSED NOW";
        }

    }

    public function getNumberOfWorkingDays()
    {
        return sizeof($this->workingDays);
    }


    private function getDayOfWeekOnSerbian($day)
    {
        if($day == "Monday"){
            return "Pon";
        }else if($day == "Tuesday"){
            return "Uto";
        }else if($day == "Wednesday"){
            return "Sre";
        }else if($day == "Thursday"){
            return "Čet";
        }else if($day == "Friday"){
            return "Pet";
        }else if($day == "Saturday"){
            return "Pet";
        }else if($day == "Sunday"){
            return "Ned";
        }else return "";
    }
}