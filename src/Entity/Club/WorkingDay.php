<?php


namespace App\Entity\Club;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Club\ClubWorkingDayRepository")
 * @ORM\Table(name="club_workig_day")
 */
class WorkingDay
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $day;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Club\Club", inversedBy="workingDays")
     */
    private $club;

     /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * @return string|null
     */
    public function getDay(): ?string
    {
        return $this->day;
    }

    /**
     * @param string $day
     */
    public function setDay(string $day): void
    {
        $this->day = $day;
    }
    
     /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     */
    public function setClub($club): void
    {
        $this->club = $club;
    }

}