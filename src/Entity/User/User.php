<?php


namespace App\Entity\User;

use App\Enum\UserRole;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation\Reservation", mappedBy="user")
     */
    private $reservations;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation\DayReservation", mappedBy="user")
     */
    private $dayReservations;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Type(type="string")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Type(type="string")
     */
    private $lastName;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Club\Club", mappedBy="user")
     */
    private $club;

    /**
     * @var string
     *
     */
    private $confirmPassword;

    public function __construct()
    {
        parent::__construct();
        $this->reservations = new ArrayCollection();
        $this->dayReservations = new ArrayCollection();
    }

    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param ArrayCollection $reservations
     */
    public function setReservations(ArrayCollection $reservations): void
    {
        $this->reservations = $reservations;
    }

    public function getDayReservations()
    {
        return $this->reservations;
    }

    /**
     * @param ArrayCollection $dayReservations
     */
    public function setDayReservations(ArrayCollection $dayReservations): void
    {
        $this->dayReservations = $dayReservations;
    }


    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     */
    public function setClub($club): void
    {
        $this->club = $club;
    }

    /**
     * @return string|null
     */
    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param string $confirmPassword
     */
    public function setConfirmPassword(string $confirmPassword): void
    {
        $this->confirmPassword = $confirmPassword;
    }

    public function getType()
    {
        if($this->getRoles()[0] == UserRole::ROLE_DEFAULT){
            return "Korisnik";
        }elseif($this->getRoles()[0] == UserRole::ROLE_OWNER){
            return "Klub";
        }else{
            return "Admin";
        }
    }

}