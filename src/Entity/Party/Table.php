<?php


namespace App\Entity\Party;

use App\Entity\Party\Image;
use App\Entity\Reservation\Reservation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Party\TableRepository")
 * @ORM\Table(name="table1")
 */
class Table
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberOfTable;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Party\Party", inversedBy="tables")
     */
    private $party;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNumberOfTable(): ?int
    {
        return $this->numberOfTable;
    }

    /**
     * @param int $barTables
     */
    public function setNumberOfTable(int $barTables): void
    {
        $this->numberOfTable = $barTables;
    }

    /**
     * @return mixed
     */
    public function getParty()
    {
        return $this->party;
    }

    /**
     * @param mixed $party
     */
    public function setParty($party): void
    {
        $this->party = $party;
    }

}