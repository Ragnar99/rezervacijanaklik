<?php


namespace App\Entity\Party;

use App\Entity\Party\Image;
use App\Entity\Reservation\Reservation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Party\PartyRepository")
 * @ORM\Table(name="party")
 */
class Party
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $barTables;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $seatingTables;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vipTables;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Club\Club", inversedBy="parties")
     */
    private $club;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation\Reservation", mappedBy="party")
     */
    private $reservations;

    /**
     * @var Image
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Party\Image", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Type(type="App\Entity\Party\Image",)
     * @Assert\Valid()
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Party\Table", mappedBy="party")
     */
    private $tables;

    /**
     * Party constructor.
     */
    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->tables = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int|null
     */
    public function getBarTables(): ?int
    {
        return $this->barTables;
    }

    /**
     * @param int $barTables
     */
    public function setBarTables(int $barTables): void
    {
        $this->barTables = $barTables;
    }

    /**
     * @return int|null
     */
    public function getSeatingTables(): ?int
    {
        return $this->seatingTables;
    }

    /**
     * @param int $seatingTables
     */
    public function setSeatingTables(int $seatingTables): void
    {
        $this->seatingTables = $seatingTables;
    }

    /**
     * @return int|null
     */
    public function getVipTables(): ?int
    {
        return $this->vipTables;
    }

    /**
     * @param int $vipTables
     */
    public function setVipTables(int $vipTables): void
    {
        $this->vipTables = $vipTables;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     */
    public function setClub($club): void
    {
        $this->club = $club;
    }
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param ArrayCollection $reservations
     */
    public function setReservations(ArrayCollection $reservations): void
    {
        $this->reservations = $reservations;
    }

    /**
     * @return null
     */
    public function getAttachDocument()
    {
        return null;
    }

    /**
     * @param null $file
     * @return array|null
     * @throws \Exception
     */
    public function setAttachDocument($file = null)
    {
        if (!$file) return [];
        $this->attachDocument($file);
        return null;
    }

    /**
     * @param UploadedFile|null $file
     * @throws \Exception
     */
    public function attachDocument(UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }
        $image = new Image();
        $image->setFile($file);
        $this->image = $image;
    }

    /**
     * @return \App\Entity\Party\Image|null
     */
    public function getImage(): ?\App\Entity\Party\Image
    {
        return $this->image;
    }

    /**
     * @param \App\Entity\Party\Image $image
     */
    public function setImage(\App\Entity\Party\Image $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTables()
    {
        return $this->tables;
    }

    /**
     * @param ArrayCollection $reservations
     */
    public function setTables(ArrayCollection $reservations): void
    {
        $this->tables = $reservations;
    }

}