<?php


namespace App\Form\Reservation;

use App\Entity\Reservation\Reservation;
use Symfony\Component\Form\AbstractType;
use App\Entity\Reservation\DayReservation;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DayReservationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stringDate', TextType::class, [
                'label' => 'Datum',
                'attr' => array(
                    'readonly' => true,
                ),
            ])
            ->add('guestNumber', NumberType::class, [
                'label' => "Broj gostiju",
                'required' => true,
                'attr'     => [
                    'class' => 'broj',
                ],
            ])
            ->add('phone', TextType::class,[
                'label' => 'Telefon',
                'required' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => DayReservation::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'site_day_reservation_type';
    }
}