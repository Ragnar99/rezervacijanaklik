<?php


namespace App\Form\Reservation;

use App\Entity\Reservation\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ReservationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('guestNumber', NumberType::class, [
                'label' => false,
                'required' => true,
                'attr'     => [
                    'class' => 'broj',
                ],
            ])
            ->add('tableType', ChoiceType::class, [
                'label' => "Tip",
                'required' => false,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (\App\Enum\TableType::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                })
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'site_reservation_type';
    }
}