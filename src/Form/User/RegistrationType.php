<?php


namespace App\Form\User;


use App\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => false,
                'attr'     => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => false,
                'attr'     => [
                    'class' => 'form-control',
                    'placeholder' => 'Ime'
                ],
            ])
            ->add('lastName', TextType::class, [
                'label' => false,
                'attr'     => [
                    'class' => 'form-control',
                    'placeholder' => 'Prezime'
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => false,
                'attr'     => [
                    'class' => 'form-control',
                    'placeholder' => 'Sifra'
                ],
            ])
            ->add('confirmPassword', PasswordType::class, [
                'label' => false,
                'attr'     => [
                    'class' => 'form-control',
                    'placeholder' => 'Sifra'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => User::class,
            'attr' => array(
                'class' => 'signin'
            )
        ]);

    }

    public function getBlockPrefix()
    {
        return 'user_registration_type';
    }
}