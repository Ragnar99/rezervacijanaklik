<?php


namespace App\Form\Search;

use App\Entity\Reservation\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GlobalSearchForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('term', TextType::class, [
                'label' => false,
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Šta tražite?',
                    'class' => "btn-group1",
                    'style' => "width:100%;"
                ],
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Bijeljina',
                    'class' => "btn-group2",
                    'style' => "width:100%;"
                ],
            ]);
    }

    public function getBlockPrefix()
    {
        return 'site_global_serach_type';
    }
}