<?php


namespace App\Form\Day;

use App\Form\Day\DayType;
use App\Entity\Club\WorkingDay;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DayType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('day', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (\App\Enum\Day::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                })
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => WorkingDay::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'owner_club_days';
    }
}