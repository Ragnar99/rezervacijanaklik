<?php


namespace App\Form\Party;

use App\Entity\Party\Party;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartyType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateTimeType::class, [
                'label' => 'Datum',
                'required' => true
            ])
            ->add('barTables', NumberType::class,[
                'label' => 'Broj barskih stolova',
                'required' => true
            ])
            ->add('seatingTables', NumberType::class,[
                'label' => 'Broj stolova sa sjedenjem',
                'required' => true
            ])
            ->add('vipTables', NumberType::class,[
                'label' => 'Broj stolova sa sjedenjem',
                'required' => true
            ])
            ->add('description', TextareaType::class,[
                'label' => 'Opis',
                'required' => true
            ])
            ->add('attachDocument', FileType::class, [
                'required' => false,
                'label' => "Slike kluba",
                'attr'     => [
                    'onchange' => 'loadFile(event)'
                ],
            ])
            ->add('name', TextType::class,[
                'label' => 'Naziv',
                'required' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Party::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'owner_party_type';
    }
}