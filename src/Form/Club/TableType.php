<?php


namespace App\Form\Club;


use App\Entity\Club\Club;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('attachDocument', FileType::class, [
                'required' => false,
                'label' => "Raspored stolova",
                'attr'     => [
                    'onchange' => 'loadFile(event)'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Club::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'table_type';
    }
}