<?php


namespace App\Form\Club;


use App\Entity\Club\Club;
use App\Enum\City;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('attachPictures', FileType::class, [
        'label'    => 'Dodaj nove slike',
        'required' => false,
        'multiple' => true,
        'attr'     => [
            'multiple' => 'multiple',
            'accept' => 'image/*',
            'onchange' => 'loadFiles(event)'
        ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Club::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'images_type';
    }
}