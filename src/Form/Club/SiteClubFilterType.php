<?php


namespace App\Form\Club;


use App\Enum\City;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteClubFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (City::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                }),
                'attr'     => [
                    'class' => 'custom-select mb-2 mr-sm-2 mb-sm-0'
                ],
            ])
            ->add('type', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (\App\Enum\ClubType::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                }),
                'attr'     => [
                    'class' => 'custom-select mb-2 mr-sm-2 mb-sm-0'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('method', 'GET')
            ->setDefault('csrf_protection', false);
    }
}