<?php


namespace App\Form\Club\Owner;


use App\Entity\Club\Club;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Naziv',
                'required' => true
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'label' => 'Adresa',
            ])
            ->add('longitude', NumberType::class, [
                'label' => 'Longitude',
                'required' => false,
            ])
            ->add('latitude', NumberType::class, [
                'label' => 'Latitude',
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'label' => 'Grad',
            ])
            ->add('barTables', NumberType::class, [
                'required' => false,
                'label' => 'Broj stolova',
            ])
            ->add('seatingTables', NumberType::class, [
                'required' => false,
                'label' => 'Broj stolova sa sjedenjem',
            ])
            ->add('vipTables', NumberType::class, [
                'required' => false,
                'label' => 'Broj secija',
            ])
            ->add('type',ChoiceType::class, [
                'label' => "Tip",
                'required' => false,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (\App\Enum\ClubType::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                })
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => 'Telefon',
            ])
            ->add('webSite', TextType::class, [
                'required' => false,
                'label' => 'Sajt',
            ])
            ->add('header', TextType::class, [
                'required' => false,
                'label' => 'Naslov',
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'Opis',
            ])
            ->add('startWorking', DateTimeType::class, [
                'label' => 'Pocetak radnog vremena',
                'required' => true
            ])
            ->add('endWorking', DateTimeType::class, [
                'label' => 'Kraj radnog vremena',
                'required' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Club::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'owner_detail_type';
    }
}