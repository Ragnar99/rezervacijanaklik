<?php


namespace App\Repository\Club;

use App\Entity\Club\WorkingDay;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ClubWorkingDayRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WorkingDay::class);
    }


}