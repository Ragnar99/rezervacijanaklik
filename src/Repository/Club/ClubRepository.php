<?php


namespace App\Repository\Club;


use App\Entity\Club\Club;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ClubRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Club::class);
    }

    /**
     * @return QueryBuilder
     */
    public function buildClubs()
    {
        $qb = $this->createQueryBuilder('club')
            ->select('club')
            ->orderBy('club.id', 'DESC');
        return $qb;
    }

    /**
     * @param null $type
     * @return QueryBuilder
     */
    public function buildClubsByType($type = null)
    {
        $qb = $this->createQueryBuilder('club')
            ->select('club')
            ->where('club.type = :type')
            ->setParameter('type', $type)
            ->orderBy('club.id', 'DESC');

        return $qb;
    }

    public function buildSearchClub($term,$city)
    {
        $qb = $this->createQueryBuilder('club')
            ->select('club');
        
        if(!is_null($term) && $term != "")
        {
            $qb->where('club.name LIKE :term')
                ->setParameter('term', '%' . $term . '%');
        }

        if(!is_null($city) && $city != "")
        {
            $qb->orWhere('club.city LIKE :city')
                ->setParameter('city', "%" . $city . "%");
        }

        $qb->orderBy('club.id', 'DESC');

        return $qb;
    }

    public function buildFilter($city, $type)
    {
        $qb = $this->createQueryBuilder('club')
            ->select('club')
            ->where('club.type LIKE :type')
            ->andWhere('club.city LIKE :city')
            ->setParameter('type', $type)
            ->setParameter('city', $city)
            ->orderBy('club.id', 'DESC');

        return $qb;
    }

}