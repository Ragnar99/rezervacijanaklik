<?php


namespace App\Repository\Party;


use App\Entity\Party\Party;
use App\Entity\Party\Table;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class TableRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Table::class);
    }

}