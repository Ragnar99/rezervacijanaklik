<?php


namespace App\Repository\Party;


use App\Entity\Party\Party;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PartyRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Party::class);
    }

    public function buildClubsByType($club, $date)
    {
        $qb = $this->createQueryBuilder('party')
            ->select('party')
            ->where('party.club = :club')
            ->andWhere('party.date > :date')
            ->setParameter('club', $club)
            ->setParameter('date', $date)
            ->orderBy('party.id', 'DESC');

        return $qb;
    }

}