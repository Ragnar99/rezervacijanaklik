<?php


namespace App\Repository\Reservation;

use App\Enum\ReservationState;
use Doctrine\ORM\Query\Expr\Join;
use App\Entity\Reservation\Reservation;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ReservationRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    /**
     * @param null $userId
     * @return QueryBuilder
     */
    public function buildUserReservations($userId = null)
    {
        $qb = $this->createQueryBuilder('reservation')
            ->select('reservation')
            ->innerJoin('reservation.user', 'user', Join::WITH)
            ->where('user.id = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('reservation.id', 'DESC');

        return $qb;
    }

     /**
     * @param null $clubId
     * @return QueryBuilder
     */
    public function buildOwnerReservations($clubId = null)
    {
        $qb = $this->createQueryBuilder('reservation')
            ->select('reservation')
            ->innerJoin('reservation.party', 'party', Join::WITH)
            ->innerJoin('party.club', 'club', Join::WITH)
            ->where('club.id = :clubId')
            ->andWhere('reservation.reservationState = :reservationState')
            ->setParameter('clubId', $clubId)
            ->setParameter('reservationState', ReservationState::pending)
            ->orderBy('reservation.id', 'DESC');

        return $qb;
    }

    public function getConfirmedReservations($party)
    {
        $qb = $this->createQueryBuilder('reservation')
            ->select('reservation')
            ->innerJoin('reservation.party', 'party', Join::WITH)
            ->where('party.id = :partyId')
            ->andWhere('reservation.reservationState = :reservationState')
            ->setParameter('partyId', $party->getId())
            ->setParameter('reservationState', ReservationState::accepted)
            ->orderBy('reservation.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

}