<?php


namespace App\Repository\Reservation;

use App\Enum\ReservationState;
use Doctrine\ORM\Query\Expr\Join;
use App\Entity\Reservation\Reservation;
use App\Entity\Reservation\DayReservation;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class DayReservationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DayReservation::class);
    }

     /**
     * @param null $userId
     * @return QueryBuilder
     */
    public function buildUserReservations($userId = null)
    {
        $qb = $this->createQueryBuilder('reservation')
            ->select('reservation')
            ->innerJoin('reservation.user', 'user', Join::WITH)
            ->where('user.id = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('reservation.id', 'DESC');

        return $qb;
    }

    /**
     * @param null $clubId
     * @return QueryBuilder
     */
    public function buildOwnerReservations($clubId = null)
    {
        $qb = $this->createQueryBuilder('reservation')
            ->select('reservation')
            ->innerJoin('reservation.club', 'club', Join::WITH)
            ->where('club.id = :clubId')
            ->andWhere('reservation.reservationState = :reservationState')
            ->setParameter('clubId', $clubId)
            ->setParameter('reservationState', ReservationState::pending)
            ->orderBy('reservation.id', 'DESC');

        return $qb;
    }

    public function getAllReservations($datetime, $clubId)
    {
        $qb = $this->createQueryBuilder('reservation')
        ->select('reservation')
        ->innerJoin('reservation.club', 'club', Join::WITH)
        ->where('club.id = :clubId')
        ->andWhere('reservation.datetime >= :datetime')
        ->andWhere('reservation.reservationState = :reservationState')
        ->setParameter('reservationState', ReservationState::accepted)
        ->setParameter('clubId', $clubId)
        ->setParameter('datetime', $datetime);

    return $qb->getQuery()->getResult();
    }

}