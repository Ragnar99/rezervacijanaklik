<?php


namespace App\Repository\Drink;


use App\Entity\Drink\Drink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr\Join;

class DrinkRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Drink::class);
    }

     /**
     * @return QueryBuilder
     */
    public function buildDrinks($id)
    {
        $qb = $this->createQueryBuilder('drink')
            ->select('drink')
            ->innerJoin('drink.club', 'club', Join::WITH)
            ->where('club.id = :clubId')
            ->setParameter('clubId', $id)
            ->orderBy('drink.id', 'DESC');
        return $qb;
    }

}