<?php


namespace App\Service;


class GenerateFileUrlService
{
    /**
     * @var string
     */
    private $fileName;

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    public function call(): bool
    {
        return sprintf(
            'https://s3.us-east-2.amazonaws.com/%s/%s',
            getenv('S3_BUCKET'),
            $this->fileName
        );
    }
}