<?php


namespace App\Service;


use Aws\Credentials\CredentialProvider;
use Aws\Exception\AwsException;
use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SaveUploadedFileService
{
    private $uploadedFile;

    /**
     * @var string
     */
    private $tmpFolderPath;

    private $imageType;


    public function setUploadedFile($uploadedFile, $imageType = null): void
    {
        $this->uploadedFile = $uploadedFile;
        $this->imageType = $imageType;
    }

    public function setTmpFolderPath(string $tmpFolderPath): void
    {
        $this->tmpFolderPath = $tmpFolderPath;
    }

    public function call()
    {

         $bucket = 'rezervacije';

        if(is_null($this->imageType)){
            $this->imageType = $this->uploadedFile->guessExtension();
            $fileName = md5(uniqid($bucket . '_', false)) . '.' . $this->imageType;

            $this->uploadedFile->move(
                    $this->tmpFolderPath,
                    $fileName
            ); 
            $this->uploadedFile = $fileName;
        }

        try {
            $s3Client = new S3Client([
                'region' => 'us-east-2',
                'version' => 'latest',
                'credentials' => CredentialProvider::env()
            ]);

            $s3Client->putObject([
                'Bucket'     => $bucket,
                'Key'        => $this->uploadedFile,
                'SourceFile' => $this->tmpFolderPath . $this->uploadedFile,
            ]);

        } catch (AwsException $e) {
            echo $e->getMessage() . "\n";
            return false;
        }

        return $this->uploadedFile;
    }
}